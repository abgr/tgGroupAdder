const apiHash = '7c40bb32dca18a094d2240d4e00970b5';
const apiId = '199134';
const os = require('os');
const path = require('path');
const dataDir = path.join(os.tmpdir(), 'abgr', 'kiarash');
const fs = require('fs');
const tdlibDir = path.join(dataDir, 'tdlib');
fs.mkdirSync(tdlibDir, { recursive: true });
const readline = require('readline');
const airgram = require('airgram');
const { prompt } = require('airgram');
let inputFilePath = '';
let outputFilePath = '';
(async () => {
    const telegramClient = new airgram.Airgram({
        useTestDc: false,
        useFileDatabase: false,
        useSecretChats: false,
        apiId, apiHash,
        useMessageDatabase: false,
        applicationVersion: 'alpha',
        databaseDirectory: tdlibDir,
        useChatInfoDatabase: true,
        deviceModel: os.platform(),
        logVerbosityLevel: 0,
        command: path.join(__dirname, '/libtdjson.so'),
        systemVersion: os.release()
    });
    telegramClient.catch((e) => {
        panic(e);
    });
    //xxx
    telegramClient.api.addProxy({ server: '5.144.128.217', port: 8000, enable: true, type: { _: 'proxyTypeSocks5' } });
    telegramClient.api.setLogStream({ _: 'logStreamEmpty' });
    console.clear();
    telegramClient.use(new airgram.Auth({
        phoneNumber: () => prompt('Please enter your phone number: '),
        code: () => prompt('Please enter the secret code: '),
        password: () => prompt('Please enter your 2FA password: ')
    }));
    let chatId = 0;
    {
        let chats = [];
        let { chatIds } = panicIfError(airgram.toObject(await telegramClient.api.getChats({ limit: 200, offsetChatId: 0, offsetOrder: '9223372036854775807' })));
        console.clear();
        for (let id of chatIds) {
            let chat = panicIfError(airgram.toObject(await telegramClient.api.getChat({ chatId: id })));
            if (chat.type._ !== 'chatTypeBasicGroup' && (chat.type._ !== 'chatTypeSupergroup' || chat.type.isChannel)) continue;
            console.log((chats.length + 1) + ')', chat.title);
            chats.push(chat.id);
        }
        if (chats.length === 0) {
            panic('no group avalaible');
        }
        selected = parseInt(await prompt('select a gourp by its index: '));
        if (isNaN(selected)) {
            panic('unknown selection');
        }
        --selected;
        if (selected >= chats.length || selected < 0) {
            panic('unknown selection');
        }
        chatId = chats[selected];
    }
    {
        inputFilePath = await prompt('enter input csv file path: ');
        if (!fs.existsSync(inputFilePath)) {
            panic(`file "${inputFilePath} not exists`);
        }
        outputFilePath = path.join(path.dirname(inputFilePath), path.basename(inputFilePath, '.csv') + '.out.csv');
    }
    const csv = readline.createInterface({
        input: fs.createReadStream(inputFilePath),
        output: null,
        console: false
    });
    let outFd = fs.openSync(outputFilePath, 'w');//xxx
    let lineIndex = -1;
    let csvEnded = false;
    csv.on('close', () => {
        csvEnded = true;
    });
    csv.on('line', async (line) => {
        csv.pause();
        console.log(0, line);
        try {
            ++lineIndex;
            console.clear();
            console.log('itr', lineIndex + 1);
            if (lineIndex <= 1) {
                fs.appendFileSync(outFd, line + os.EOL);
            } else {
                line = line.toString();
                if (line.length < 8) throw new Error(`csv error: file "${inputFilePath}" line ${lineIndex}`);
                let row = line.split(',');
                if (row.length < 2) {
                    row.push('UNKNOWN');
                }
                try {
                    row[0] = normalizePhone(row[0]);
                } catch (e) {
                    console.log(lineIndex);
                    throw new Error(`phone error: phone "${row[0]}" file "${inputFilePath}" line ${lineIndex}`);
                }
                let { userIds: [userId] } = panicIfError(airgram.toObject(await telegramClient.api.importContacts({ contacts: [{ phoneNumber: row[0] }] })));
                try {
                    await telegramClient.api.clearImportedContacts();
                } catch (_) { }
                if (userId == 0) {
                    row[1] = 'USER_NOT_FOUND';
                } else {
                    try {
                        let e = takeArigramError(airgram.toObject(await telegramClient.api.addChatMember({ chatId, userId })));
                        if (e) throw e;
                        row[1] = '';
                    } catch (e) {
                        row[1] = e.message;
                    }
                }
                fs.appendFileSync(outFd, row.join(',') + os.EOL);
            }
        } catch (e) {
            panic(e);
        } finally {
            if (csvEnded) {
                fs.fsyncSync(outFd);
                fs.closeSync(outFd);
                process.exit(0);
            } else {
                csv.resume();
            }
        }
    });
})
    ().catch(e => {

        console.error(e.message || e);
    });
function takeArigramError(response) {
    if (response && typeof response._ === 'string' && response._.toLowerCase().indexOf('error') > -1) {
        e = new Error(response.message);
        e.code = response.code;
        return e;
    }
}
function panicIfError(response = {}) {
    let e = takeArigramError(response);
    if (e) panic(e);
    return response;
}
function responseTypeOf(response) {
    return response._;
}
let phoneRegexes = {
    reg989364306699: { reg: /^\d{12}$/, fn: (phone) => { return '+' + phone; } },
    regPlus989364306699: { reg: /^\+\d{12}$/, fn: (phone) => { return phone; } },
    reg00989364306699: { reg: /^00\d{12}$/, fn: (phone) => { return '+' + phone.slice(2); } },
    reg09364306699: { reg: /^0\d{10}$/, fn: (phone) => { return '+98' + phone.slice(1); } },
    reg9364306699: { reg: /^\d{10}$/, fn: (phone) => { return '+98' + phone; } }
};
function normalizePhone(phone = '') {
    phone = phone.replace(/\s/g, '');
    for (let i in phoneRegexes) {
        if (phoneRegexes[i].reg.test(phone)) {
            return phoneRegexes[i].fn(phone);
        }
    }
    throw new Error('unsupported phone number format');
}
function panic(e) {
    console.log(e.message || e);
    process.exit(2);
}
