const apiHash = '';
const apiId = '';
const { Auth, Airgram, toObject, prompt } = require('airgram');
const path = require('path');
const os = require('os');
const fs = require('fs');
const readline = require('line-by-line');
let telegramClient = new Airgram({
    useTestDc: false,
    useFileDatabase: false,
    useSecretChats: false,
    apiId, apiHash,
    useMessageDatabase: false,
    applicationVersion: 'alpha',
    databaseDirectory: path.join(os.tmpdir(), 'abgr', 'kiarash', 'tdlib'),
    useChatInfoDatabase: true,
    deviceModel: os.platform(),
    logVerbosityLevel: 0,
    command: path.join(__dirname, '/libtdjson.so'),
    systemVersion: os.release()
});
process.on('beforeExit', telegramClient.api.close.bind(telegramClient));
telegramClient.catch((e) => {
    console.error(e);
    process.exit(2);
});
//xxx
telegramClient.api.addProxy({ server: '', port: 8000, enable: true, type: { _: 'proxyTypeSocks5' } });
telegramClient.api.setLogStream({ _: 'logStreamEmpty' });
telegramClient.use(new Auth({
    phoneNumber: async () => {
        console.clear();
        let r = await prompt('Please enter your phone number: ');
        console.log('wait...');
        return r;
    },
    code: async () => {
        let r = await prompt('Please enter the sent code: ');
        console.log('wait...');
        return r;
    },
    password: async () => {
        let r = await prompt('Please enter your 2FA password ');
        console.log('wait...');
        return r;
    }
}));
let started = false;
telegramClient.on('updateConnectionState', async ({ update: { state: { _: state } } }) => {
    if (state === 'connectionStateReady' && !started) {
        started = true;
        try {
            await ready({ telegramClient });
        } catch (e) {
            console.error(e);
            process.exit(1);
        }
    }
});
let phoneRegexes = {
    reg989364306699: { reg: /^\d{12}$/, fn: (phone) => { return '+' + phone; } },
    regPlus989364306699: { reg: /^\+\d{12}$/, fn: (phone) => { return phone; } },
    reg00989364306699: { reg: /^00\d{12}$/, fn: (phone) => { return '+' + phone.slice(2); } },
    reg09364306699: { reg: /^0\d{10}$/, fn: (phone) => { return '+98' + phone.slice(1); } },
    reg9364306699: { reg: /^\d{10}$/, fn: (phone) => { return '+98' + phone; } }
};
function normalizePhone(phone = '') {
    phone = phone.replace(/\s/g, '');
    for (let i in phoneRegexes) {
        if (phoneRegexes[i].reg.test(phone)) {
            return phoneRegexes[i].fn(phone);
        }
    }
    throw new Error('unsupported phone number format');
}
async function ready({ telegramClient }) {
    console.clear();
    let chatId = await selectGroup({ telegramClient });
    let filePath = await selectCsvFile();
    let input = new readline(filePath);
    input.pause();
    let lineCount = 0;
    let output = fs.openSync(path.join(path.dirname(filePath), path.basename(filePath, '.csv') + '.out.csv'), 'w');///xxxx
    input.on('line', async (line) => {
        try {
            //console.clear();
            console.log(lineCount == 0 ? '' : lineCount - 1);
            let lineIndex = lineCount++;
            input.pause();
            if (lineIndex === 0) {
                fs.appendFileSync(output, line + os.EOL);
            } else {
                if (line.length <= 8) panic(`file error line ${lineIndex + 1}`);

                let row = line.split(',');
                if (row.length < 2) row.push('UNKNOWN');
                try {
                    row[0] = normalizePhone(row[0]);
                } catch (e) {
                    panic(`phone error: line ${lineIndex + 1}`);
                }
                let { _, message, code, userIds: [userId] } = toObject(await telegramClient.api.importContacts({ contacts: [{ phoneNumber: row[0] }] }));
                if (_ === 'error') {
                    panic(message);
                }
                if (userId === 0) {
                    row[1] = 'USER_NOT_FOUND';
                } else {
                    try {
                        let { _, message } = toObject(await telegramClient.api.addChatMember({ chatId, userId }));
                        if (_ === 'error') {
                            throw new Error(message);
                        }
                        row[1] = 'USER_ADDED_SUCCESSFULLY';
                    } catch (e) {
                        row[1] = e.message;
                    }
                }

                fs.appendFileSync(output, row.join(',') + os.EOL);
            }
            setTimeout(input.resume.bind(input), 10);
        } catch (e) {
            if (e.code === 429) {
                try {
                    console.log(e.message);
                    setTimeout(() => {
                        --lineCount;
                        input.emit('line', line);
                    }, ((Math.floor(Math.random() * 30) + 10) + parseInt(e.message.match(/\d{1,}/gm)[0])) * 1000);
                    return;
                } catch (e) {
                    panic(e);
                }
            }
            else panic(e);
        } finally {
            try {
                await telegramClient.api.clearImportedContacts();
            } catch (_) { }
            fs.fsyncSync(output);
        }

    });
    input.on('close', async () => {
        console.log('done');
        if ((await prompt('logout? (y/n)')).toLowerCase().startsWith('y')) {
            await telegramClient.api.logout();
        }
        await telegramClient.api.close();
    });
    input.resume();
}
function panic(e) {
    console.error(e.message || e);
    process.exit(1);
}
function pass() { }
async function selectCsvFile() {
    console.clear();
    for (let i = 0; i < 3; i++) {
        let path = await prompt('enter input csv file: ');
        try {
            fs.closeSync(fs.openSync(path, 'r'));
            return path;
        } catch (e) {
            console.log(e.message);
        }
    }
    throw new Error('error file');
}
async function selectGroup({ telegramClient }) {
    console.clear();
    let { _, message, chatIds } = toObject(await telegramClient.api.getChats({ limit: 999, offsetChatId: 0, offsetOrder: '9223372036854775807' }));
    if (_ === 'error') {
        throw new Error(message);
    } else {
        let groups = [];
        for (let chatId of chatIds) {
            let { _, id, title, message, type: { _: type, isChannel } } = toObject(await telegramClient.api.getChat({ chatId }));
            if (_ === 'error') {
                throw new Error(message);
            } else {
                if (type !== 'chatTypeBasicGroup' && (type !== 'chatTypeSupergroup' || isChannel)) continue;
                console.log((groups.length + 1) + ')', title);
                groups.push(id);
            }
        }
        if (groups.length === 0) {
            throw new Error('no group available');
        }
        for (let i = 0; i < 3; i++) {
            let index = parseInt(await prompt('select a group by its index: '));
            --index;
            if (index >= 0 && index < groups.length) {
                return groups[index];
            }
            console.log('unknown index');
        }
        throw new Error('unknown index');
    }
}